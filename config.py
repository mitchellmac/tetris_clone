import os
basedir = os.path.abspath(os.path.dirname(__file__))
sqlite_uri = 'sqlite:///' + os.path.join(basedir, 'app.dir')

class Config(object):
  SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or sqlite_uri
  SQLALCHEMY_TRACK_MODIFICATIONS = False
