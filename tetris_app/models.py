from tetris_app import db
from datetime import datetime


class User(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  username = db.Column(db.String(64), index=True, unique=True)
  password_hash = db.Column(db.String(128))
  scores = db.relationship('Score', backref='player', lazy='dynamic')

  def __repr__(self):
    return '<User {}>'.format(self.username)


class Score(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  score = db.Column(db.Integer)
  timestamp = db.Column(db.DateTime, default=datetime.utcnow)
  user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

  def __repr__(self):
    return '<Score {}>'.format(self.score)
