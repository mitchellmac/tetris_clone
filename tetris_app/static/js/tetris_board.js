class GameBoard {
    constructor(width, height) {
        this.width = width + 2;
        this.height = height;
        this.buildBoard();
    }

    buildBoard() {
        // creates an empty game board
        this.board = []
        for (var i = 0; i < this.height; i++) {
            this.board.push(new Array(this.width).fill(0));
            // Setting up boundary edges of board
            this.board[i][0] = 1;
            this.board[i][this.width - 1] = 1;
        }
        // adding a bottom edge to the board
        this.board.push(new Array(this.width).fill(1));
    }

    rowFull(row_idx) {
        // Returns true if the queried row does not include 0. Otherwise false.
        return !this.board[row_idx].includes(0);
    }

    printBoard() {
        // prints a string representation of board to console.
        var boardString = "";
        for (var row = 0; row < this.height; row++) {
            var rowvals = "";
            for (var col = 0; col < this.width - 2; col++) {
                rowvals += this.board[row][col + 1] + " ";
            }
            boardString += rowvals + "\n";
        }
        console.log(boardString);
    }
}
