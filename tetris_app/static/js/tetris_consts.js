/*
Global constants
*/

const pieceTypes = [
    'T',
    'Square',
    'Stick',
    'L',
    'J',
    'RDog',
    'LDog'
];

const pieceRotations = {
    'T': [
        [[0, "purple", 0],
         ["purple", "purple", "purple"],
         [0, 0, 0]],
        [[0, "purple", 0],
         [0, "purple", "purple"],
         [0, "purple", 0]],
        [[0, 0, 0],
         ["purple", "purple", "purple"],
         [0, "purple", 0]],
        [[0, "purple", 0],
         ["purple", "purple", 0],
         [0, "purple", 0]]
    ],
    'Square': [
        [[0, " #f1c40f ", " #f1c40f "],
         [0, " #f1c40f ", " #f1c40f "]]
    ],
    'Stick': [
        [[0, 0, 0, 0],
         ["#5dade2", "#5dade2", "#5dade2", "#5dade2"],
         [0, 0, 0, 0],
         [0, 0, 0, 0]],
        [[0, 0, "#5dade2", 0],
         [0, 0, "#5dade2", 0],
         [0, 0, "#5dade2", 0],
         [0, 0, "#5dade2", 0]],
        [[0, 0, 0, 0],
         [0, 0, 0, 0],
         ["#5dade2", "#5dade2", "#5dade2", "#5dade2"],
         [0, 0, 0, 0]],
        [[0, "#5dade2", 0, 0],
         [0, "#5dade2", 0, 0],
         [0, "#5dade2", 0, 0],
         [0, "#5dade2", 0, 0]]
    ],
    'L': [
        [[0, 0, "#C71585"],
         ["#C71585", "#C71585", "#C71585"],
         [0, 0, 0]],
        [[0, "#C71585", 0],
         [0, "#C71585", 0],
         [0, "#C71585", "#C71585"]],
        [[0, 0, 0],
         ["#C71585", "#C71585", "#C71585"],
         ["#C71585", 0, 0]],
        [["#C71585", "#C71585", 0],
         [0, "#C71585", 0],
         [0, "#C71585", 0]],
        
    ],
    'J': [
        [["blue", 0, 0],
         ["blue", "blue", "blue"],
         [0, 0, 0]],
        [[0, "blue", "blue"],
         [0, "blue", 0],
         [0, "blue", 0]],
        [[0, 0, 0],
         ["blue", "blue", "blue"],
         [0, 0, "blue"]],
        [[0, "blue", 0],
         [0, "blue", 0],
         ["blue", "blue", 0]]
    ],
    'LDog': [
        [["green", "green", 0],
         [0, "green", "green"],
         [0, 0, 0]],
        [[0, 0, "green"],
         [0, "green", "green"],
         [0, "green", 0]],
        [[0, 0, 0],
         ["green", "green", 0],
         [0, "green", "green"]],
        [[0, "green", 0],
         ["green", "green", 0],
         ["green", 0, 0]]
    ],
    'RDog': [
        [[0, "red", "red"],
         ["red", "red", 0],
         [0, 0, 0]],
        [[0, "red", 0],
         [0, "red", "red"],
         [0, 0, "red"]],
        [[0, 0, 0],
         [0, "red", "red"],
         ["red", "red", 0]],
        [["red", 0, 0],
         ["red", "red", 0],
         [0, "red", 0]]
    ]
};

// pieceKicks holds the kick test order for pieces. So for an I piece
// in rotation state 1, kick test order would be pieceKicks[1][1].
const pieceKicks = [
    [  // For JLSTZ
        [[0, -1], [1, -1], [-2, 0], [-2, -1]],
        [[0, -1], [-1, -1], [2, 0], [2, -1]],
        [[0, 1], [1, 1], [-2, 0], [-2, 1]],
        [[0, 1], [-1, 1], [2, 0], [2, 1]]
    ], 
    [  // For I
        [[0, 1], [0, -2], [2, 1], [-1, -2]],
        [[0, -2], [0, 1], [1, -2], [-2, 1]],
        [[0, -1], [0, 2], [-2, -1], [1, 2]],
        [[0, 2], [0, -1], [-1, 2], [2, -1]]
    ],
    [  // For S
        []
    ]
];

const pieceTypeToKicks = {
    'T': pieceKicks[0],
    'L': pieceKicks[0],
    'J': pieceKicks[0],
    'RDog': pieceKicks[0],
    'LDog': pieceKicks[0],
    'Stick': pieceKicks[1],
    'Square': pieceKicks[2]
};

const keyCodeToMove = {
    37: [0, -1],  // left
    38: "rotate",  // up => rotate clockwise
    39: [0, 1],  // right
    40: [1, 0],  //down
    80: "pause",  // letter "p" for pause
    27: "pause"  // esc also pauses
}

const levelToFramesPerFall = [
    50, 46, 42, 38, 34, 30, 25, 20, 15, 11, 10, 9, 8, 7, 6, 6, 5, 5, 4, 4
]

const rowsClearedToScore = {0: 0, 1: 40, 2: 100, 3: 300, 4: 1200};