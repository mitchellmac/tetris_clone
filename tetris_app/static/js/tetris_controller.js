
function* pieceTypeGen() {
    // Generates sequences of random permutations of tetris piece types.
    var randPieces = [];
    while (true) {
        if (randPieces.length === 0) {
            randPieces = shuffle(pieceTypes.slice());
        }
        yield randPieces.pop();
    }
}


class GameController {
    constructor() {
        // Default board dimensions are 10x20
        this.gameBoard = new GameBoard(10, 20);
        this.pieceTypeGenerator = pieceTypeGen();
        this.paused = false;
        this.activePiece = null;
        this.nextPiece = null;
        this.moveQueue = [];
        this.level = 0;
        this.levelRowsCleared = 0;
        this.totalRowsCleared = 0;
        this.numUpdates = 0;
        this.score = 0;
        this.interval = null;
        this.renderer = new Renderer(
            document.getElementById("gameArea").getContext("2d")
        )
        this.renderer.drawInitialState();
    }

    startGame() {
        this.renderer.drawInitialScores(
            this.score, this.level + 1, this.totalRowsCleared
        );

        this.activePiece = this.spawnPiece();
        this.nextPiece = this.spawnPiece();
        this.putPieceOnBoard(this.activePiece);
        this.renderer.drawNextPiece(this.nextPiece);

        // makes eventlistener function refer to this gamecontroller instead 
        // of window.
        var gc = this;
        window.addEventListener("keydown", function(e) {
            if (keyCodeToMove[e.keyCode] !== undefined) {
                gc.moveQueue.push(keyCodeToMove[e.keyCode]);
            }
        });

        // game will be updated 50 times per second.
        // arrow function ensures binding 'this' to gameController instead of
        // window.
        this.interval = setInterval(() => this.updateGame(), 20);
    }

    spawnPiece() {
        var pieceType = this.pieceTypeGenerator.next().value;
        var spawnPos = [0, 4];
        return new GamePiece(spawnPos, pieceType);
    }

    updateGame() {
        // Every second (50 updates) the piece is forced to fall one space. 
        // Otherwise execute the next move in the move queue.

        this.numUpdates++;
        if (this.numUpdates === levelToFramesPerFall[this.level]) {
            // Forced fall logic
            var fall = [1, 0];
            if (this.validMove(this.activePiece, fall)) {
                // Make the piece fall a row if it's a valid move.
                this.movePiece(this.activePiece, fall);
            } else {
                // Piece can't move down. Clear any filled lines, spawn a
                // piece, and check for game over.
                var numRowsCleared = this.clearRows();
                this.updateScore(numRowsCleared);
                this.updateLevel(numRowsCleared);

                this.activePiece = this.nextPiece;
                this.nextPiece = this.spawnPiece();
                this.renderer.drawNextPiece(this.nextPiece);
                if (!this.validPlacement(this.activePiece)) {
                    this.gameOver();
                } else {
                    this.putPieceOnBoard(this.activePiece);
                    this.moveQueue = [];
                }
            }
            // re-initialize move queue and update counter.
            this.numUpdates = 0;
        } else if (this.moveQueue.length > 0) {
            // Movement / rotation logic
            var move = this.moveQueue.shift();
            if (move === "rotate") {
                this.rotate(this.activePiece);
            } else if (move == "pause") {
                clearInterval(this.interval);
                showPauseMenu();
                document._onkeydownHolder = document.onkeydown;
                document.onkeydown = null;
                this.moveQueue = [];
            } else if (this.validMove(this.activePiece, move)) {
                this.movePiece(this.activePiece, move);

                // Soft fall score calculation
                if (move[0] === 1 && move[1] == 0) {
                    this.score++;
                    this.renderer.drawScore(this.score);
                }
            }
        }
        this.renderer.drawGameBoard(this.gameBoard);
    }

    validMove(piece, move) {
        for (var r = 0; r < piece.state.length; r++) {
            for (var c = 0; c < piece.state[0].length; c++) {
                var moveRow = r + move[0];
                var moveCol = c + move[1];
                
                // For non-zero parts, the new position only needs to be
                // checked if the new position is not already occupied by part
                // of this piece.
                if (piece.state[r][c] !== 0 &&
                    moveRow < piece.state.length &&
                    moveRow >= 0 &&
                    moveCol < piece.state[0].length &&
                    moveCol >= 0 &&
                    piece.state[moveRow][moveCol] !== 0) {
                        continue;
                } else if (piece.state[r][c] !== 0) {
                    // Checking if the new board position is already occupied.
                    var boardRow = piece.topLeft[0] + moveRow;
                    var boardCol = piece.topLeft[1] + moveCol;
                    if (this.gameBoard.board[boardRow][boardCol] !== 0) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    movePiece(piece, move) {
        this.removePieceFromBoard(piece);
        piece.topLeft = [piece.topLeft[0] + move[0], piece.topLeft[1] + move[1]];
        this.putPieceOnBoard(piece);
    }

    clearRows() {
        var numRowsCleared = 0;

        // Clears full rows and moves all above rows down a level.
        for (var r = 0; r < this.gameBoard.height; r++) {
            if (this.gameBoard.rowFull(r)) {
                var replacementRow = new Array(this.gameBoard.width).fill(0);
                replacementRow[0] = 1;
                replacementRow[this.gameBoard.width - 1] = 1;

                // moves the previous rows down a level and replaces the top
                // row with an empty row.
                this.gameBoard.board.splice(
                    0, r + 1,
                    replacementRow, ...this.gameBoard.board.slice(0, r)
                );
                
                numRowsCleared++;
            }
        }
        return numRowsCleared;
    }

    updateScore(numRowsCleared) {
        this.score += rowsClearedToScore[numRowsCleared] * (this.level + 1);
        this.renderer.drawScore(this.score);
    }

    updateLevel(numRowsCleared) {
        this.levelRowsCleared += numRowsCleared;
        this.totalRowsCleared += numRowsCleared;
        this.renderer.drawLines(this.totalRowsCleared);
        if (this.levelRowsCleared >= 10) {
            this.levelRowsCleared -= 10;
            this.level = Math.min(this.level + 1, 20);
            this.renderer.drawLevel(this.level + 1);
        }
    }

    validPlacement(piece) {
        // Makes sure that you can place the active piece on the board.
        for (var r = 0; r < piece.state.length; r++) {
            for (var c = 0; c < piece.state[0].length; c++) {
                if (piece.state[r][c] !== 0) {
                    var boardRow = piece.topLeft[0] + r;
                    var boardCol = piece.topLeft[1] + c;
                    if (this.gameBoard.board[boardRow][boardCol] !== 0) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    gameOver() {
        clearInterval(this.interval);
        console.log(this.score);
    }

    putPieceOnBoard(piece) {
        for (var i = 0; i < piece.state.length; i++) {
            for (var j = 0; j < piece.state[0].length; j++) {
                if (piece.state[i][j] != 0) {
                    var row = piece.topLeft[0] + i;
                    var col = piece.topLeft[1] + j;
                    this.gameBoard.board[row][col] = piece.state[i][j];
                }
            }
        }
    }

    rotate(piece) {
        this.removePieceFromBoard(piece);
        piece.rotateClockwise();

        var kick = this.getKick(piece);
        if (kick !== null) {
            piece.topLeft[0] += kick[0];
            piece.topLeft[1] += kick[1];
        } else {
            piece.rotateCounterClockwise();
        }

        this.putPieceOnBoard(piece);
    }

    getKick(piece) {
        // Attempts to 'kick' a piece into the right position following a 
        // rotation. Returns a move if the corresponding kick is valid, or
        // returns null if no kicks are valid.

        // No kick needed!
        if (this.validPlacement(piece)) {
            return [0, 0];
        }

        var kicks = pieceTypeToKicks[piece.pieceType][piece.rotationIdx];
        for (const kick of kicks) {
            if (this.validMove(piece, kick)) {
                return kick;
            }
        }
        // No kicks were valid :(
        return null;
    }

    removePieceFromBoard(piece) {
        for (var i = 0; i < piece.state.length; i++) {
            for (var j = 0; j < piece.state[0].length; j++) {
                if (piece.state[i][j] != 0) {
                    var row = piece.topLeft[0] + i;
                    var col = piece.topLeft[1] + j;
                    this.gameBoard.board[row][col] = 0;
                }
            }
        }
    }

    resumeGame() {
        this.interval = setInterval(() => this.updateGame(), 20);
        document.onkeydown = document._onkeydownHolder;
        document._onkeydownHolder = null;
    }
}
