class GamePiece {
    constructor(topLeft, pieceType) {
        this.topLeft = topLeft;
        this.pieceType = pieceType;
        this.rotations = pieceRotations[pieceType];
        this.rotationIdx = 0;
        this.state = this.rotations[this.rotationIdx];
    }

    rotateClockwise() {
        this.rotationIdx = mod(this.rotationIdx + 1, this.rotations.length);
        this.state = this.rotations[this.rotationIdx];
    }

    rotateCounterClockwise() {
        this.rotationIdx = mod(this.rotationIdx - 1, this.rotations.length);
        this.state = this.rotations[this.rotationIdx];
    }
}
