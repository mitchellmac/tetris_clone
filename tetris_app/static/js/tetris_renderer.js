class Renderer {
    // Game canvas is 700 x 640 with a centered 300 x 600 play area. Score
    // displays are on the left and the next piece is on the right.
    constructor(context) {
        this.context = context;

        this.backgroundColor = "gray";
        this.displayWidth = context.canvas.clientWidth;
        this.displayHeight = context.canvas.clientHeight;

        // variables for rendering play area of canvas
        this.boardAnchor = [199, 20];
        this.gameWidth = 302;
        this.gameHeight = 602;
        this.boardColor = "black";
        this.lineWidth = 2;
        this.blockWidth = (this.gameWidth - 11 * this.lineWidth) / 10;
        this.blockStrokeColor = "white";

        // Text style attributes (Font is shared)
        this.context.font = "30px Arial";
        this.labelTextFillColor = "red";
        this.labelTextStrokeColor = "black";
        this.labelTextAlignment = "left";
        this.scoreTextFillColor = "white";
        this.scoreTextAlignment = "right";

        // Label text anchors (left aligned)
        this.nextPieceTextAnchor = [570, 60];
        this.scoreTextAnchor = [60, 60];
        this.levelTextAnchor = [60, 150];
        this.linesTextAnchor = [60, 240];

        // Score anchors (right aligned)
        this.scoreAnchor = [165, 100];
        this.levelAnchor = [165, 190];
        this.linesAnchor = [165, 280];

        // Score / next piece container attributes
        this.containerColor = "black";
        this.scoreContainersDims = [150, 40];
        this.nextPieceContainerDims = [150, 90];
        this.scoreContainerAnchor = [25, 70];
        this.levelContainerAnchor = [25, 160];
        this.linesContainerAnchor = [25, 250];
        this.nextPieceContainerAnchor = [525, 70];
    }

    drawInitialState() {
        // draw background
        this.context.fillStyle = this.backgroundColor;
        this.context.fillRect(0, 0, this.displayWidth, this.displayHeight);

        // draw label text
        this.context.fillStyle = this.labelTextFillColor;
        this.context.strokeStyle = this.labelTextStrokeColor;
        this.context.textAlign = this.labelTextAlignment;
        this.context.fillText("Score", ...this.scoreTextAnchor);
        this.context.strokeText("Score", ...this.scoreTextAnchor);
        this.context.fillText("Level", ...this.levelTextAnchor);
        this.context.strokeText("Level", ...this.levelTextAnchor);
        this.context.fillText("Lines", ...this.linesTextAnchor);
        this.context.strokeText("Lines", ...this.linesTextAnchor);
        this.context.fillText("Next", ...this.nextPieceTextAnchor);
        this.context.strokeText("Next", ...this.nextPieceTextAnchor);

        // draw containers
        this.context.fillStyle = this.containerColor;
        this.context.fillRect(
            ...this.scoreContainerAnchor,
            ...this.scoreContainersDims
        );
        this.context.fillRect(
            ...this.levelContainerAnchor,
            ...this.scoreContainersDims
        );
        this.context.fillRect(
            ...this.linesContainerAnchor,
            ...this.scoreContainersDims
        );
        this.context.fillRect(
            ...this.nextPieceContainerAnchor,
            ...this.nextPieceContainerDims
        );

        // draw play area
        this.context.fillStyle = this.boardColor;
        this.context.fillRect(
            ...this.boardAnchor, 
            this.gameWidth, this.gameHeight
        );
    }

    drawInitialScores(score, level, lines) {
        this.drawScore(score);
        this.drawLevel(level);
        this.drawLines(lines);
    }

    drawContainer(anchor, dims) {
        this.context.fillStyle = this.containerColor;
        this.context.fillRect(...anchor, ...dims);
    }

    drawGameText(text, anchor) {
        this.context.textAlign = this.scoreTextAlignment;
        this.context.fillStyle = this.scoreTextFillColor;
        this.context.fillText(text, ...anchor);
    }

    drawScore(score) {
        this.drawContainer(this.scoreContainerAnchor, this.scoreContainersDims);
        this.drawGameText(score, this.scoreAnchor);
    }

    drawLevel(level) {
        this.drawContainer(this.levelContainerAnchor, this.scoreContainersDims);
        this.drawGameText(level, this.levelAnchor);
    }

    drawLines(lines) {
        this.drawContainer(this.linesContainerAnchor, this.scoreContainersDims);
        this.drawGameText(lines, this.linesAnchor);
    }

    drawNextPiece(piece) {
        this.eraseNextPiece();

        // Offset for centering in the next piece display area depends 
        // on the type of piece.
        var centeringOffsets;
        if (piece.pieceType == 'Stick') {
            centeringOffsets = [15, 0];
        } else if (piece.pieceType == 'Square') {
            centeringOffsets = [15, 15];
        } else {
            centeringOffsets = [30, 15];
        }

        for (var row_idx = 0; row_idx < piece.state.length; row_idx++) {
            for (var col_idx = 0; col_idx < piece.state[0].length; col_idx++) {
                if (piece.state[row_idx][col_idx] !== 0) {
                    this.drawNextPieceBlock(
                        row_idx, col_idx, 
                        piece.state[row_idx][col_idx], centeringOffsets
                    );
                }
            }
        }
    }

    eraseNextPiece() {
        this.context.fillStyle = this.containerColor;
        this.context.fillRect(
            ...this.nextPieceContainerAnchor,
            ...this.nextPieceContainerDims
        );
    }

    drawNextPieceBlock(row_idx, col_idx, color, offsets) {
        var x = col_idx * (this.lineWidth + this.blockWidth);
        x += this.nextPieceContainerAnchor[0] + offsets[0];
        var y = row_idx * (this.lineWidth + this.blockWidth);
        y += this.nextPieceContainerAnchor[1] + offsets[1];

        this.drawBlock(x, y, color);
    }

    drawGameBoard(gameBoard) {
        this.eraseBoard();

        for (var row_idx = 0; row_idx < gameBoard.height; row_idx++) {
            for (var col_idx = 1; col_idx < gameBoard.width - 1; col_idx++) {
                if (gameBoard.board[row_idx][col_idx] !== 0) {
                    this.drawGameBlock(
                        row_idx, col_idx,
                        gameBoard.board[row_idx][col_idx]
                    );
                }
            }
        }
    }

    eraseBoard() {
        this.context.fillStyle = this.boardColor;
        this.context.fillRect(
           ...this.boardAnchor, 
            this.gameWidth, this.gameHeight
        );
    }

    drawGameBlock(row_idx, col_idx, color) {
        // Calculate the x, y position of the block using the provided index
        // and the board anchor
        var x = (col_idx - 1) * (this.lineWidth + this.blockWidth);
        x += this.boardAnchor[0];
        var y = row_idx * (this.lineWidth + this.blockWidth);
        y += this.boardAnchor[1];

        this.drawBlock(x, y, color);
    }

    drawBlock(x, y, color) {
        // Draw block outline
        this.context.fillStyle = this.blockStrokeColor;
        this.context.fillRect(
            x, y,
            this.blockWidth + 2 * this.lineWidth, this.blockWidth + 2 * this.lineWidth
        );

        // Draw the block inside the "outline"
        this.context.fillStyle = color;
        this.context.fillRect(
            x + this.lineWidth, y + this.lineWidth,
            this.blockWidth, this.blockWidth
        );
    }
}
