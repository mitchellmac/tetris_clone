/*
Sets up page UI
*/
var gameController = new GameController();
var menuWrapper = document.getElementById("menu");
var mainMenu = document.getElementById("main");
var pauseMenu = document.getElementById("pause");
var levelMenu = document.getElementById("levelSelect");

function hide(el) {
    el.style.display = "none";
}

function show(el) {
    el.style.display = "block";
}

function showMainMenu() {
    show(mainMenu);
}

function showPauseMenu() {
    show(menuWrapper);
    show(pauseMenu);
}

// Button function for starting the game
document.querySelectorAll('.start')[0].addEventListener('click', function() {
    hide(mainMenu);
    hide(menuWrapper);
    gameController.startGame();
});

//  Function for resuming the game after pausing
document.querySelectorAll('.resume')[0].addEventListener('click', function() {
    hide(pauseMenu);
    hide(menuWrapper);
    setTimeout(() => gameController.resumeGame(), 2000);
})

// Function for opening the level select menu
document.querySelectorAll(".level")[0].addEventListener("click", function() {
    hide(mainMenu);
    show(levelMenu);
});

// Function for confirming level selection choice
document.querySelectorAll(".levelSelect")[0].addEventListener("click", function() {
    var selectedLevel = document.getElementById("levelChoice").value;
    gameController.level = parseInt(selectedLevel) - 1;
    hide(levelMenu);
    show(mainMenu);
});

// Function for quitting the game
document.querySelectorAll(".quit")[0].addEventListener("click", function() {
    gameController = new GameController();
    hide(pauseMenu);
    show(mainMenu);
});

showMainMenu();
