/*
Math utils
*/

function mod(a, b) {
    // accurately takes mods of negative numbers.
    // usage: mod(a, b) is a % b.
    return ((a % b) + b) % b;
}

function shuffle(arr) {
    for (var i = arr.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
    return arr;
}