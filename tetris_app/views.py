from tetris_app import app


@app.route('/')
def tetris():
  return render_template('tetris.html')
